let foo = [];
import {Cache} from 'cache-base';

export function getRandomData() {
	
	let column = 10;
    let row = 100;
    let i = 0;
    let j = 0;
    let rowData = [];
    let tableData = [];

  
  app = new Cache();
  

    for(i = 0; i < row; i++) {
      rowData = [];
      for(j = 0; j < column; j++) {
        rowData.push(parseFloat((Math.random() * 100).toFixed(2)));
      }
      tableData.push(rowData);
    }

    if (foo.length == 0) {
      foo = tableData;
      app.set('data', tableData);
    }
    console.log(app.get('data'));
	return tableData;
};

export function getFoo(){

  if (foo.length == 0) {
    getRandomData();
  }
  return foo;
}