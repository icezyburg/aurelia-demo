export class App {
  configureRouter(config, router){
    config.title = 'Contacts';
    config.map([
      { 
        route: ['', 'home'], 
        moduleId: 'components/home/home', 
        name: 'home', 
        nav: true,
        title: 'Home'
      },
      { route: ['a'], moduleId: 'components/a/a', name: 'a', nav: true, title: 'Menu A'},
      { route: ['b'], moduleId: 'components/b/b', name: 'b', nav: true, title: 'Menu B'},
      { route: ['c'], moduleId: 'components/c/c', name: 'c', nav: true, title: 'Menu C'},
      { route: ['d'], moduleId: 'components/d/d', name: 'd', nav: true, title: 'Menu D'}
    ]);

    this.router = router;
  }
}
